const app = getApp();
let reqData = {};

reqData = {
  // 电影数据
  getIndex: function () {
    return new Promise((resolve, reject)=>{
      let time = new Date().getTime();
      wx.request({
        url: 'https://m.maizuo.com/v4/api/film/coming-soon',
        data: {
          __t: time,
          page: 1,
          count: 5
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          // console.log(res)
          resolve(res.data.data.films)
        },
        fail: function (res) { },
        complete: function (res) { },
      })
    })
  },
  // 正在热映电影
  getPlayMoive: function (page, count) {
    return new Promise((resolve, reject) => {
      let time = new Date().getTime();
      wx.request({
        url: 'https://m.maizuo.com/v4/api/film/now-playing',
        data: {
          __t: time,
          page,
          count
        },
        method: 'GET',
        dataType: 'json',
        success: ({ data }) => {
          // console.log(data,data.data.page)
          resolve({
            result: data.data.films,
            total: data.data.page.total
          })
        },
        fail: function (res) { },
        complete: function (res) { },
      })
    })
  },
  // 电影详情
  detailMoive: function (id,time) {
    return new Promise((resolve, reject) => {
      let time = new Date().getTime();
      wx.request({
        url: `https://m.maizuo.com/v4/api/film/${id}`,
        data: {
          __t: time
        },
        method: 'GET',
        dataType: 'json',
        success: (res) => {
          // console.log(res)
          resolve(res.data.data.film)
        },
        fail: function (res) { },
        complete: function (res) { },
      })
    })
  },
};

module.exports = reqData;