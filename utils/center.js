const center = {} //中间对象
// 监听
center.on = function(eventName, callback){
  if(!center[eventName]){
    center[eventName] = []
  }
  // 保存回调函数
  center[eventName].push(callback)
}
// 触发
center.emit = function(eventName, params){
  // 取出对应事件的回调函数
  let callbackArr = center[eventName]
// 调用每一个回调事件
  callbackArr.map((callback)=>{
    callback(params)
  })
  // console.log(center)
}
// 移除监听
center.off = function(eventName, callback){
  // 情况1：只传evenName，移除所有事件
  // 情况2：传eventName和callback
  if(!callback) {
    center[eventName] = null
  } else {
    let callbackArr = center[eventName]
    // 删除指定的callback回调
    let result = callbackArr.filter(callbackItem=>{
      if (callback == callbackItem) {
        return false
      } else {
        return true
      }
    })
    // 重新赋值
    center[eventName] = result

    // console.log(center)
  }
}

export default center