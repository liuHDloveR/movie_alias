let movieTmp = null;

function setMovie(info){
  movieTmp = info;
}

function getMovie () {
  return movieTmp
}

export default {
  setMovie,
  getMovie
}