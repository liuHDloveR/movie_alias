// pages/comment/comment.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectedImg:[],
    canSelet: true,
    isActive: false,
    recordflie: '',
    videoFlie: '',
    address: ''
  },
  // 添加图片
  addimg:function(){
    // 选择照片
    // 最多选取九张
    // 取出多少，还可以选多少
    let count = 9 - this.data.selectedImg.length;

    wx.chooseImage({
      count,
      success:({tempFilePaths})=>{
        let images = this.data.selectedImg.concat(tempFilePaths)
        // 判断是否为九张，是就不显示加号
        if (images.length>=9) {
          this.setData({
            canSelet: false,
            selectedImg: images
          }) 
        } else {
          this.setData({
            selectedImg: images
          })
        }
        // console.log(this.data.selectedImg)
      }
    })
  },

  // 预览图片
  previewImgAction: function (ev) {
    let { index } = ev.currentTarget.dataset;
    console.log(index)
    wx.previewImage({
      current: index,
      urls: this.data.selectedImg,
    })
  },
  // 删除照片
  deleteImg: function (ev) {
    let { index } = ev.currentTarget.dataset;
    this.data.selectedImg.splice(index, 1);
    this.setData({
      selectedImg: this.data.selectedImg,
      canSelet: true
    })
  },
  // 开始录音
  msnuiStart:function() {
    this.setData({
      isActive: true
    });
    console.log(1)
    wx.startRecord({
      success: ({ tempFilePath})=>{
        this.setData({
          recordfile: tempFilePath
        })
      }
    });
  },
  // 结束录音
  msnuiEnd: function () {
    this.setData({
      isActive: false
    })
    wx.stopRecord()
  },
  // 播放录音
  playmsnui: function(){
    wx.playVoice({
      filePath: this.data.recordfile
    })
  },
  // 开始录制视频
  startVideo: function(){
    wx.chooseVideo({
      maxDuration: 10,
      success: ({tempFilePath}) => {
        console.log(tempFilePath)
        this.setData({
          videoFlie: tempFilePath
        })
      }
    })
  },
  // 定位
  getLocation:function(){
    // wx.getLocation({
    //   success:(res)=>{
    //     console.log(res)
    //     wx.openLocation({
    //       latitude: res.latitude,
    //       longitude: res.longitude,
    //     })
    //   }
    // })

    wx.chooseLocation({
      success: (res) => {
        console.log(res)
        this.setData({
          address: res.address
        })
      },
    })
  },
  // 提交评论
  commitInfo: function(){
    console.log(this.data);
    // 判断网络是否可用
    new Promise((reslove, reject)=>{
      wx.getNetworkType({
        success: (res) => { 
          if (res.networkType == 'none') {
            
          } else {
             reslove()
          }
        },
      })
    }).then(()=>{

    }).catch(()=>{
      let { selectedImg, recordflie, videoFlie, address } = this.data;
      // 持久化零时文件存储
      wx.saveFile({
        tempFilePath: '',
        success: (res)=>{

        }
      })
      wx.setStorage({
        key: 'tmpComment',
        data: {
          selectedImg,
          recordflie,
          videoFlie,
          address
        },
      })
    })
    

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.stopVoice()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})