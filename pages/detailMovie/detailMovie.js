// pages/detailMovie/detailMovie.js
import { detailMoive } from '../../utils/request.js'
import tmp from '../../utils/tmp.js'

let movieId = '';
let show = false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showComment: show
  },
  // 去评论页面
  addcomment: function () {
    wx.navigateTo({
      url: '../comment/comment',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    movieId = options.id;
    show = options.flag == 'playing';
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 获得电影信息
    let movieTmp = tmp.getMovie();
    wx.setNavigationBarTitle({title:movieTmp.name})
    console.log(movieTmp)
    detailMoive(movieId).then(
      (result) => {
        console.log(result)
      })

    this.setData({
      showComment: show
    })

    wx.getStorage({
      key: 'tmpComment',
      success: function(res) {
        console.log(res);
      },
    })
  }
})